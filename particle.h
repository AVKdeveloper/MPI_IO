#ifndef MPI_IO_PARTICLE_H_
#define MPI_IO_PARTICLE_H_

#include <stdlib.h>

typedef struct particle_t {
    int x;
    int y;
    int r;  // characteristic of particle
} particle_t;

particle_t *particle_create(size_t source, size_t area_size, size_t a,
        size_t b);
void particle_destroy(particle_t *particle);
size_t particle_get_area(particle_t *particle, size_t area_size, size_t a,
        size_t b);

#endif  // MPI_IO_PARTICLE_H_
