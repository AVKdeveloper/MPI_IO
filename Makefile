COMPILER = mpicc
CFLAGS = -Wall -Werror -std=c99

run: main.o list.o particle.o cells.o
	$(COMPILER) $(CFLAGS) main.o list.o particle.o cells.o -o run
main.o: main.c
	$(COMPILER) $(CFLAGS) main.c -c
test: unit_tests.o list.o particle.o cells.o
	$(COMPILER) $(CFLAGS) unit_tests.o list.o particle.o cells.o -o test
unit_tests.o: unit_tests.c
	$(COMPILER) $(CFLAGS) unit_tests.c -c
list.o: list.c list.h
	$(COMPILER) $(CFLAGS) list.c -c
particle.o: particle.c particle.h
	$(COMPILER) $(CFLAGS) particle.c -c
cells.o: cells.c cells.h
	$(COMPILER) $(CFLAGS) cells.c -c
clean:
	rm *.o run test core.* slurm-* stats.txt data.bin
