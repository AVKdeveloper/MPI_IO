#include "cells.h"

#include <assert.h>
#include <stdlib.h>
#include <stdio.h>

cell_t *cell_create(int x, int y, int mask_size, int *mask) {
    cell_t *cell = (cell_t *)malloc(sizeof(cell_t));
    cell->x = x;
    cell->y = y;
    cell->mask_size = mask_size;
    cell->mask = mask;
    for (int i = 0; i < mask_size; ++i) {
        cell->mask[i] = 0;
    }
    return cell;
}

void cell_destroy(cell_t *cell) {
    free(cell);
}

int cell_get_order(cell_t *cell, int area_size, int a, int b) {
    return get_order_by_coordinates(cell->x, cell->y, a, b, area_size);
}

int get_order_by_coordinates(int x, int y, int a, int b, int area_size) {
    return x + y * b * area_size;
}

area_t *area_create(int a, int b, int rank, int area_size, int mask_size) {
    area_t *area = (area_t *)malloc(sizeof(area_t));
    area->a = a;
    area->b = b;
    area->rank = rank;
    area->area_size = area_size;
    area->cells = (cell_t **)malloc(area_size * area_size * sizeof(cell_t *));
    area->mask = (int *)malloc(area_size * area_size * mask_size * sizeof(int));
    int area_x = rank % b * area_size;
    int area_y = rank / b * area_size;
    for (int i = 0; i < area_size * area_size; ++i) {
        int local_x = i % area_size;
        int local_y = i / area_size;
        area->cells[i] = cell_create(area_x + local_x, area_y + local_y,
                mask_size, &area->mask[i * mask_size]);
    }
    return area;
}

void area_destroy(area_t *area) {
    for (int i = 0; i < area->area_size * area->area_size; ++i) {
        cell_destroy(area->cells[i]);
    }
    free(area->mask);
    free(area->cells);
    free(area);
}

cell_t *area_get_cell(area_t *area, int x, int y) {
    int local_x = x % area->area_size;
    int local_y = y % area->area_size;
    int idx = local_y * area->area_size + local_x;
    return area->cells[idx];
}

void area_account_particle(area_t *area, particle_t *particle) {
    cell_t *cell = area_get_cell(area, particle->x, particle->y);
    cell->mask[particle->r]++;
}

void area_print_masks(area_t *area) {
    printf("Area with rank %d:\n", area->rank);
    for (int i = 0; i < area->area_size * area->area_size; ++i) {
        cell_t *cell = area->cells[i];
        printf("cell #%d: x=%d y=%d mask ", cell_get_order(cell,
            area->area_size, area->a, area->b), cell->x, cell->y);
        assert(area_get_cell(area, cell->x, cell->y) == cell);
        for (int j = 0; j < cell->mask_size; ++j) {
            printf("%d ", cell->mask[j]);
        }
        printf("\n");
    }
    const int mask_size = area->cells[0]->mask_size;
    printf("maks_size=%d\n", mask_size);
    printf("Area mask:\n");
    for (int i = 0; i < area->area_size * area->area_size * mask_size; ++i) {
        printf("%d ", area->mask[i]);
        if ((i + 1) % mask_size == 0) {
            printf("\n");
        }
    }
}
