#include "particle.h"

#include <assert.h>
#include <stdio.h>

particle_t *particle_create(size_t source, size_t area_size, size_t a,
        size_t b) {
    particle_t *particle = (particle_t *)malloc(sizeof(particle_t));
    particle->x = source % b * area_size + rand() % area_size;
    particle->y = source / b * area_size + rand() % area_size;
    particle->r = rand() % (a * b);
    return particle;
}

void particle_destroy(particle_t *particle) {
    free(particle);
}

size_t particle_get_area(particle_t *particle, size_t area_size, size_t a,
        size_t b) {
    return particle->y / area_size * b + particle->x / area_size;
}
