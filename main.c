#include <assert.h>
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "cells.h"
#include "list.h"
#include "particle.h"

int main(int argc, char **argv) {
    MPI_Init(&argc, &argv);
    int world_rank, world_size;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    if (argc != 5) {
        fprintf(stderr, "Wrong number of arguments!\n");
        MPI_Abort(MPI_COMM_WORLD, 1);
    }
    const int l = atoi(argv[1]);
    if (l < 1) {
        fprintf(stderr, "Wrong length of area side!\n");
        MPI_Abort(MPI_COMM_WORLD, 1);
    }
    const int a = atoi(argv[2]);
    const int b = atoi(argv[3]);
    if (a < 1 || b < 1) {
        fprintf(stderr, "Wrong number of areas!\n");
        MPI_Abort(MPI_COMM_WORLD, 1);
    }
    if (a * b != world_size) {
        fprintf(stderr, "Number of areas isn't equal to number of nodes!\n");
        MPI_Abort(MPI_COMM_WORLD, 1);
    }
    const int N = atoi(argv[4]);
    if (N < 1) {
        fprintf(stderr, "Wrong number of particles in each area!\n");
        MPI_Abort(MPI_COMM_WORLD, 1);
    }
    const int mask_size = a * b;

    // Starting time
    double start_time = MPI_Wtime();
    srand(time(NULL) + world_rank * 100);

    // Set view for file
    MPI_Aint intex;
    MPI_Aint lb;
	// MPI_Type_extent(MPI_INT, &intex);
    MPI_Type_get_extent(MPI_INT, &lb, &intex);
    MPI_File fh;
	MPI_File_open(MPI_COMM_WORLD, "data.bin", MPI_MODE_CREATE | MPI_MODE_WRONLY,
            MPI_INFO_NULL, &fh);
    MPI_Datatype view;
	MPI_Type_vector(l, l * mask_size, l * b * mask_size, MPI_INT, &view);
	MPI_Type_commit(&view);
    const int x_position = world_rank % b;
    const int y_position = world_rank / b;
    const int offset = intex * (x_position * l + y_position * b * l * l)
            * mask_size;
    MPI_File_set_view(fh, offset, MPI_INT, view, "native", MPI_INFO_NULL);

    // Counting particles
    list_t *list = list_create(N);
    for (int i = 0; i < N; ++i) {
        particle_t *particle = particle_create(world_rank, l, a, b);
        list_add(list, particle);
    }
    area_t *area = area_create(a, b, world_rank, l, mask_size);
    while (list_size(list) > 0) {
        particle_t *particle = list_remove(list, NULL);
        area_account_particle(area, particle);
        particle_destroy(particle);
    }

    // MPI write to file
    MPI_File_write_all(fh, area->mask, l * l * mask_size, MPI_INT,
            MPI_STATUS_IGNORE);
    MPI_Type_free(&view);
	MPI_File_close(&fh);

    MPI_Barrier(MPI_COMM_WORLD);
    // The 0 node is responsible for statistics
    if (world_rank == 0) {
        double end_time = MPI_Wtime();
        FILE* file = fopen("stats.txt", "w");
        fprintf(file, "%d %d %d %d %fs\n", l, a, b, N, end_time - start_time);
        fclose(file);
    }

    area_destroy(area);
    list_destroy(list);
    MPI_Finalize();
    return 0;
}
