#ifndef MPI_IO_CELLS_H_
#define MPI_IO_CELLS_H_

#include "particle.h"

typedef struct cell_t {
    int x;
    int y;
    int mask_size;
    int *mask;
} cell_t;

cell_t *cell_create(int x, int y, int mask_size, int *mask);
void cell_destroy(cell_t *cell);
int cell_get_order(cell_t *cell, int area_size, int a, int b);
int get_order_by_coordinates(int x, int y, int a, int b, int area_size);

typedef struct area_t {
    int a;
    int b;
    int rank;
    int area_size;
    int *mask;
    cell_t **cells;
} area_t;

area_t *area_create(int a, int b, int rank, int area_size, int mask_size);
void area_destroy(area_t *area);
cell_t *area_get_cell(area_t *area, int x, int y);
void area_account_particle(area_t *area, particle_t *particle);
void area_print_masks(area_t *area);

#endif  // MPI_IO_CELLS_H_
