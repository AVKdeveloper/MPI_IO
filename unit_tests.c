#include <assert.h>
#include <limits.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "list.h"
#include "particle.h"
#include "cells.h"

void test1();
void test2();
void test3();
void test4();

int main() {
    printf("Starting tests\n");
    test1();
    test2();
    test3();
    test4();
    printf("All tests passed successfully!\n");
    return 0;
}

void test1() {
    printf("Test for list_t\n");
    list_t *list = list_create(10);
    list_node_t *node;
    int element1 = 1;
    int element2 = 4;
    int element3 = 2;
    assert(list_size(list) == 0);
    assert(list_get_next_node(list, NULL) == NULL);
    list_add(list, &element1);
    assert(list_size(list) == 1);
    node = list_get_next_node(list, NULL);
    assert(*(int *)(node->data) == 1);
    list_add(list, &element2);
    assert(list_size(list) == 2);
    node = list_get_next_node(list, NULL);
    assert(*(int *)(node->data) == 4);
    node = list_get_next_node(list, node);
    assert(*(int *)(node->data) == 1);
    node = list_get_next_node(list, node);
    assert(node == NULL);
    list_add(list, &element3);
    assert(list_size(list) == 3);
    assert(*(int *)list_remove(list, NULL) == 2);
    assert(list_size(list) == 2);
    node = list_get_next_node(list, NULL);
    assert(*(int *)(node->data) == 4);
    assert(*(int *)list_remove(list, node) == 1);
    assert(list_size(list) == 1);
    node = list_get_next_node(list, NULL);
    assert(*(int *)(node->data) == 4);
    list_add(list, &element1);
    assert(list_size(list) == 2);
    node = list_get_next_node(list, NULL);
    assert(*(int *)(node->data) == 1);
    node = list_get_next_node(list, node);
    assert(*(int *)(node->data) == 4);
    assert(node->next == NULL);
    list_destroy(list);
    printf("OK\n");
}

void test2() {
    printf("Test for particle_t\n");
    for (int i = 0; i < 100; ++i) {
        particle_t *particle = particle_create(i, 10, 25, 4);
        assert(particle_get_area(particle, 10, 25, 4) == i);
        particle_destroy(particle);
    }
    printf("OK\n");
}

void test3() {
    printf("Test for cells\n");
    int a = 2;
    int b = 3;
    int rank = 4;
    int area_size = 4;
    int mask_size = a * b;
    area_t *area = area_create(a, b, rank, area_size, mask_size);
    area_print_masks(area);
    area_destroy(area);
    printf("OK\n");
}

void test4() {
    printf("Test for accounting_particles\n");
    int N = 1000;
    int a = 2;
    int b = 3;
    int world_rank = 4;
    int l = 4;
    int mask_size = a * b;

    list_t *list = list_create(N);
    for (int i = 0; i < N; ++i) {
        particle_t *particle = particle_create(world_rank, l, a, b);
        list_add(list, particle);
    }
    area_t *area = area_create(a, b, world_rank, l, mask_size);
    while (list_size(list) > 0) {
        particle_t *particle = list_remove(list, NULL);
        area_account_particle(area, particle);
        particle_destroy(particle);
    }

    area_print_masks(area);

    area_destroy(area);
    list_destroy(list);

    printf("OK\n");
}
