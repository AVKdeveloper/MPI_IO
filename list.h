#ifndef RANDOM_WALK_MPI_LIST_H_
#define RANDOM_WALK_MPI_LIST_H_

#include <stdlib.h>

typedef struct list_node_t {
    void *data;
    struct list_node_t *next;
} list_node_t;

list_node_t *list_node_create(void *data);
void list_node_destroy(list_node_t *node);

typedef struct list_t {
    list_node_t *head;
    size_t size;
    list_node_t *head_free;
    size_t capacity;
    list_node_t *all_nodes;
} list_t;

list_t *list_create(size_t capacity);
void list_destroy(list_t *list);
size_t list_size(const list_t *list);
void list_add(list_t *list, void *data);
list_node_t *list_get_next_node(list_t *list, list_node_t *previous);
void *list_remove(list_t *list, list_node_t *previous);

#endif  // RANDOM_WALK_MPI_LIST_H_
